#Onyx

<h2>Yes, this is us.</h2>
If you are wondering if this is the new Shitkik, you arent mistaken.

<h2>Why was everything changed AGAIN?</h2>
Well, we started making Shitkik-v2, and realized that, well, Shitkik doesn't exactly sound good. We then realized that EVERY DAMN MODDED KIK ENDS IN EITHER *IK OR *KIK. We are here to change that, to start a revolution, to rebuild Kik from the ground up with every design aspect new. We don't want Onyx to be seen as "just another take on a more popular modded kik" and "has the same features just a slightly different design", because that is a waste of time. What we want Onyx to be seen as a new type of messaging app, compatible with the Kik network, one could say "Kik as if it were reborn, a new, revolutionary app", but we all know Kik Interactive will never do anything like that, so that's where we come in... 

<h2>Meet Onyx.</h2>

#What is Onyx?

Onyx is a new messaging app, currently supporting Kik, but soon to support many others. Onyx is an app built from the ground up. Onyx plans to be the first unified & modified messaging app. All of the most popular messaging apps (only exception being apps that have a propietary UI, like Snapchat). Onyx is more than just a modded client for Kik, it is a revolution in the messaging app world, no more having somebody say "add me on x" then having to get whatever platform they use, login or create an account, just add your account or create one with ease using a unified platform, meet Onyx.

